Get Contentful data and return as a javascript object.

## About

This tool solves the problem of dowloading Contentful entires via the Contentful Delivery API and passing to templates during a static site build. 
[Contentful](https://www.contentful.com) is a content management platform for websites, web applications, mobile apps and connected devices. It allows
you to create, edit & manange content in the cloud and publish it anywhere via a powerful API.

### Features

- Retrieve all entries via Contentful's [Content Delivery API](https://www.contentful.com/developers/docs/references/content-delivery-api/).
- Filter entries by [Content Type](https://www.contentful.com/developers/docs/concepts/data-model/).
- Specify level of [Link resolution](https://www.contentful.com/developers/docs/concepts/links/).
- Returns javascript object of all entries for easy insertion into static site templates like [pug](https://pugjs.org).
- Works with [Gulp](http://gulpjs.com).

### Requirements

- node.js (https://nodejs.org)

## Getting Started

To use contentful-data you will need to install it and get credentials in order to access your content in Contentful.

### Installation

In node, using [npm](http://npmjs.org):

``` sh
npm install contentful-data
```

### Authentication

In order to get content from Contentful, you have to pass in your credentials as params, which are handed off to [Contentful's Javascript SDK](https://www.npmjs.com/package/contentful) under the hood.

To create API keys, login to [Contentful](https://app.contentful.com), open the space from which you want to download content (top left corner lists all the spaces), and navigate to the APIs area. 
Open the API Keys section and create your first token. You will also need your Space ID.

For more information, refer to Contentful's REST API reference on [Authentication](https://www.contentful.com/developers/docs/references/authentication/).

## Basic Usage (Gulp)
``` js
'use strict';

var gulp = require('gulp');
var contentfulData = require('contentful-data');

// Global object to store returned data.
var entries = null;

gulp.task('data', function(cb){
  // Only get entries once per build. Prevents numerous API calls if build run often after a gulp watch, for example.  
  var params = {
    apiKey: 'your-api-key',
    spaceId: 'your-space-id'
  };

  return contentfulData(params, function(err, data){
    if(!err) {
      entries = data;
      cb();
    }
  });
});

gulp.task('default', ['data'], function(){
  console.log('Retrieved contenful entries.');
});
```

### Params

| name | type | description |
|------|------|-------------|
| `apiKey` | string | A production Content Delivery API [key](https://www.contentful.com/developers/docs/references/content-delivery-api/#/introduction/authentication). |
| `spaceId` | string | Alphanumeric id of the space to retrieve. |
| `opts` | object | Object of available options. |
| `opts.filter` | array | Array of content types to get. If no filter is passed, all content types will be downloaded. |
| `opts.level` | number | Number of [links](https://www.contentful.com/developers/docs/references/content-delivery-api/#/reference/links) to resolve. |   
| `opts.key` | string | The id of the field to use for identifying an entry (e.g. `slug`). If no key is supplied, the default `sys.id` of the entry will be used instead. |
| `opts.locale` | string | the value of locales requested from the contentful space. If no key is supplied, the default key, representing the default language of the space will be used instead. |

## Examples 

### Gulp with Pug

This tool was primarily meant to work in a Gulp build using pug templates to generate a static site.

``` js
var contentfulData = require('contentful-data');
var pug = require('pug');
var entries = null;

gulp.task('data', function(cb){
  // Only get entries once per build. Prevents numerous API calls if build run often after a gulp watch, for example.
  if (!entries) {
    
    var params = {
      apiKey: 'your-api-key',
      spaceId: 'your-space-id',
      opts: {
        level: 2
      }
    };

    return contentfulData(params, function(err, data){
      if(!err) {
        entries = data;
        cb();
      } else {
        console.error('error:' + err);
      }
    });
  } else {
    cb();
  }
})

gulp.task('pug', ['data'], function(cb){
  for (var pageId in entries.page){

    // Assign nav and page data to template
    var locals = {
      navs: entries.navList,
      page: entries.page[pageId]
    }

    // Get pug template to use.
    var template = entries.page[pageId].fields.template;
    
    // Compile pug template
    var compiled = pug.compileFile('./src/templates/' + template + '.pug')(locals);
    
    // Define path to output template
    var path = 'path/to/output/compiled/template';
    path = path + '/' + locals.page.fields.slug;
    mkpath.sync(path);
    
    // Write compiled pug template
    var filename = path + '/index.html';
    fs.writeFileSync(filename,  compiled);
  }

  cb();
});

gulp.task('default', ['pug'], function(){
  console.log('Watching files for changes.');

  gulp.watch(['path-to-pug-templates'], ['pug']);
});
```

## License

MIT